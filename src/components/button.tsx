import {PropsWithChildren} from "react";

export default function Button(props: PropsWithChildren<{onClick: () => {}}>) {
    return <button onClick={props.onClick} style={{
        padding: 5,
        border: '2px solid #eee',
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
        cursor: 'pointer',
        outline: 'aliceblue',
    }}>{props.children}</button>
}
