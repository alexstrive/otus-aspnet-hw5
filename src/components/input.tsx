import {PropsWithoutRef} from "react";

export default function Input(props: PropsWithoutRef<{
    value: string,
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
}>) {
    return <input value={props.value} onChange={props.onChange} style={{
        width: 200,
        padding: 5,
        borderRadius: 5,
        border: "2px solid #eee",
        boxShadow: 'none',
        borderBottomRightRadius: 0,
        borderTopRightRadius: 0,
        backgroundColor: 'transparent',
    }}></input>
}
