import React, {useCallback, useMemo, useState} from 'react';
import Input from "./components/input";
import Button from "./components/button";
import hljs from 'highlight.js/lib/core';
import 'highlight.js/styles/github.css';
import json from 'highlight.js/lib/languages/json';
import getJson from "./service";
import axios from "axios";
import "./main.css"

hljs.registerLanguage('json', json);

function App() {
    const [responseData, setResponseData] = useState('')
    const [url, setUrl] = useState('https://catfact.ninja/fact')
    const codeToRender = useMemo(() => hljs.highlight(
        JSON.stringify(responseData, null, 2),
        {language: 'json'}
    ).value, [responseData])

    const handleButtonClick = useCallback(async () => {
        try {
            const resp = await getJson(url);
            setResponseData(resp.data)
        } catch (e) {
            if (axios.isAxiosError(e)) {
                console.log(e)
            }
        }

    }, [url]);

    const handleUrlChange = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
        setUrl(e.target.value)
    }, [])

    return (
        <div>
            <main>
                <div>
                    <Input value={url} onChange={handleUrlChange}/>
                    <Button onClick={handleButtonClick}>
                        Отправить
                    </Button>
                </div>
                <pre style={{maxWidth: 800, textWrap: 'wrap', backgroundColor: '#fafafa', padding: 20, borderRadius: 5}}>
                    <code>
                        <div dangerouslySetInnerHTML={{__html: codeToRender}}/>
                    </code>
                </pre>
            </main>
        </div>
    );
}

export default App;
