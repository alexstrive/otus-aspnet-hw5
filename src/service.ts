import axios from "axios";

export default function getJson(url: string) {
    return axios.get(url)
}
